﻿-- Opcion 3 - Cardinalidad 'N' - '1'
-- [Ejemplar]-- N -- [Presta] -- 1 -- [Socio]

-- Con tabla auxiliar
CREATE DATABASE IF NOT EXISTS opcion3_n1;
USE opcion3_n1;

CREATE TABLE ejemplar(
  cod_ejemplar varchar(30),
  PRIMARY KEY(cod_ejemplar)
);

CREATE TABLE socio(
  cod_socio varchar(30),
  PRIMARY KEY(cod_socio)
);

CREATE TABLE presta(
  ejemplar varchar(30),
  socio varchar(30),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY(ejemplar,socio),
  UNIQUE KEY(ejemplar),
  CONSTRAINT fk_PrestaEjemplar FOREIGN KEY (ejemplar)
    REFERENCES ejemplar(cod_ejemplar) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_PrestaSocio FOREIGN KEY (socio)
    REFERENCES socio(cod_socio) on DELETE CASCADE ON UPDATE CASCADE 
);