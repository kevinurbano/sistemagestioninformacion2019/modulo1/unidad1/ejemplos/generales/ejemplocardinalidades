﻿-- Opcion 4 - Cardinalidad '1' - '1'
-- [Ejemplar]-- 1 -- [Presta] -- 1 -- [Socio]

-- Con tabla auxiliar
CREATE DATABASE IF NOT EXISTS opcion4_11;
USE opcion4_11;

CREATE TABLE ejemplar(
  cod_ejemplar varchar(30),
  PRIMARY KEY(cod_ejemplar)
);

CREATE TABLE socio(
  cod_socio varchar(30),
  PRIMARY KEY(cod_socio)
);

CREATE TABLE presta(
  ejemplar varchar(30),
  socio varchar(30),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY(ejemplar,socio),
  UNIQUE KEY(ejemplar),
  UNIQUE KEY(socio),
  CONSTRAINT fk_PrestaEjemplar FOREIGN KEY (ejemplar)
    REFERENCES ejemplar(cod_ejemplar) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_PrestaSocio FOREIGN KEY (socio)
    REFERENCES socio(cod_socio) on DELETE CASCADE ON UPDATE CASCADE 
);