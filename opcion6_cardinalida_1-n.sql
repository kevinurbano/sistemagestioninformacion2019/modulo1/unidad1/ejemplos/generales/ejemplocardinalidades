﻿-- Opcion 6 - Cardinalidad '1' - 'N'
-- [Ejemplar]-- 1 -- [Presta] -- N -- [Socio]

-- Sin tabla auxiliar (propagada)
CREATE DATABASE IF NOT EXISTS opcion6_1n;
USE opcion6_1n;

CREATE TABLE ejemplar(
  cod_ejemplar varchar(30),
  PRIMARY KEY(cod_ejemplar)
);


CREATE TABLE socio(
  cod_socio varchar(30),
  ejemplar varchar(30),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY(cod_socio),
  CONSTRAINT fk_SocioEjemplar FOREIGN KEY (ejemplar)
    REFERENCES ejemplar(cod_ejemplar) ON DELETE CASCADE ON UPDATE CASCADE
);

