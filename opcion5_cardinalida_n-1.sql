﻿-- Opcion 5 - Cardinalidad 'N' - '1'
-- [Ejemplar]-- N -- [Presta] -- 1 -- [Socio]

-- Sin tabla auxiliar (propagada)
CREATE DATABASE IF NOT EXISTS opcion5_n1;
USE opcion5_n1;

CREATE TABLE socio(
  cod_socio varchar(30),
  PRIMARY KEY(cod_socio)
);

CREATE TABLE ejemplar(
  cod_ejemplar varchar(30),
  socio varchar(30),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY(cod_ejemplar),
  CONSTRAINT fk_EjemplarSocio FOREIGN KEY (socio)
    REFERENCES socio(cod_socio) ON DELETE CASCADE ON UPDATE CASCADE
);


